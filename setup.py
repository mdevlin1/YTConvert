from cx_Freeze import setup, Executable

base = None


executables = [Executable("app.py", base=base)]

packages = ["idna"]
options = {
    'build_exe': {

        'packages':packages,
    },

}

setup(
    name = "YTConvert",
    options = options,
    version = "1.0",
    description = 'YouTube Batch Converter',
    executables = executables
)
